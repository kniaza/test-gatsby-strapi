/* eslint-disable comma-dangle */
/* eslint-disable space-before-function-paren */
// /**
//  * Implement Gatsby's Node APIs in this file.
//  *
//  * See: https://www.gatsbyjs.org/docs/node-apis/
//  */

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const result = await graphql(
    `
      {
        products: allStrapiProducts {
          edges {
            node {
              strapiId
              slug
            }
          }
        }
      }
    `
  )

  if (result.errors) {
    throw result.errors
  }

  // Create blog articles pages.
  const products = result.data.products.edges

  const ProductTemplate = require.resolve("./src/templates/product.template.js")

  products.forEach((product, index) => {
    createPage({
      path: `/product/${product.node.slug}`,
      component: ProductTemplate,
      context: {
        slug: product.node.slug,
      },
    })
  })
}

module.exports.onCreateNode = async ({ node, actions, createNodeId }) => {
  const crypto = require("crypto")

  if (node.internal.type === "StrapiProducts") {
    const newNode = {
      id: createNodeId(`StrapiProductsContent-${node.id}`),
      parent: node.id,
      children: [],
      internal: {
        content: node.content || " ",
        type: "StrapiProductsContent",
        mediaType: "text/markdown",
        contentDigest: crypto
          .createHash("md5")
          .update(node.content || " ")
          .digest("hex"),
      },
    }
    actions.createNode(newNode)
    actions.createParentChildLink({
      parent: node,
      child: newNode,
    })
  }
}
