/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it

import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import "./src/styles/slick-slider-custom.css"
import "react-toastify/dist/ReactToastify.css"
import { ProductProvider } from "./src/helpers/storage/productContext"
import { ToastContainer } from "react-toastify"
import React from "react"

export const wrapPageElement = ({ element }) => {
  return (
    <ProductProvider>
      {element}
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </ProductProvider>
  )
}
