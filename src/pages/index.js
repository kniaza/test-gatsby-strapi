/** @jsx jsx */
import React from "react"

import Layout from "../components/Layout"
import SEO from "../components/SEO"
import Hero from "../components/HomePage/Hero"
import { Container } from "../components/Grid"
import Slider from "../components/Common/Slider"
import { Styled, jsx } from "theme-ui"
import { graphql } from "gatsby"

export default function IndexPage({ data }) {
  const { edges } = data.products
  return (
    <Layout>
      <SEO title="Home" />
      <Container>
        <Hero />

        <main sx={styles.main_container}>
          <div sx={styles.heading_container}>
            <Styled.h2 sx={styles.heading}>Explore community choices</Styled.h2>
            <Styled.p sx={styles.heading_paragraph}>
              Updated daily based on most popular choices among dev community
            </Styled.p>
          </div>

          <Slider data={edges} />
        </main>
      </Container>
    </Layout>
  )
}

export const data = graphql`
  query AllProductsFromStrapi {
    products: allStrapiProducts {
      edges {
        node {
          id
          excerpt
          slug
          price
          name
        }
      }
    }
  }
`

const styles = {
  main_container: {
    my: 200,
  },
  heading_container: {
    mb: 60,
    ml: 20,
  },
  heading: {
    fontFamily: "body",
    fontWeight: "bold",
  },
  heading_paragraph: {
    maxWidth: 320,
    color: "light",
  },
}
