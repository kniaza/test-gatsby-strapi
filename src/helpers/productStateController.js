export const addCartItem = (product, useLocalstorageSetter) => {
  useLocalstorageSetter(prevState => [...prevState, product])
}

export const removeCartItem = (product, useLocalstorageSetter) => {
  useLocalstorageSetter(prevState => {
    return prevState.filter(item => item.id !== product.id)
  })
}
