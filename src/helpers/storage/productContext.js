/* eslint-disable comma-dangle */
import React, { createContext, useEffect, useState } from "react"
import useLocalStorage from "../../hooks/useLocalStorage.hook"
import { addCartItem, removeCartItem } from "../productStateController"

export const ProductContext = createContext([[], () => {}])

export const ProductProvider = ({ children }) => {
  const [products, setProducts] = useState([])
  const [productsLocalStorage, setProductsLocalStorage] = useLocalStorage(
    "products",
    []
  )

  useEffect(() => {
    setProducts(productsLocalStorage)
  }, [productsLocalStorage])

  return (
    <ProductContext.Provider
      value={{
        products,
        actions: {
          addCartItem: product => addCartItem(product, setProductsLocalStorage),
          removeCartItem: product =>
            removeCartItem(product, setProductsLocalStorage),
        },
      }}
    >
      {children}
    </ProductContext.Provider>
  )
}
