/** @jsx jsx */
import { jsx } from "theme-ui"
import { Container } from "../components/Grid"
import Layout from "../components/Layout"
import SEO from "../components/SEO"
import ProductImage from "../images/products/dumy.svg"
import { ProductContext } from "../helpers/storage/productContext"

export default function ProductPage({ data }) {
  const { product } = data

  return (
    <ProductContext.Consumer>
      {({ products, actions }) => {
        const isChoosedProduct =
          products.filter(item => item.id === product.id).length > 0
        return (
          <Layout>
            <SEO title="Product" />

            <Container sx={styles.content}>
              <div sx={styles.productImageContainer}>
                <img
                  sx={styles.productImage}
                  src={ProductImage}
                  alt="product image"
                />
              </div>
              <div sx={styles.productInfo}>
                <div sx={styles.tagsContainer}>
                  {product.tags.map(tag => {
                    return (
                      <span key={tag.id} sx={styles.tagItem}>
                        {tag.name}
                      </span>
                    )
                  })}
                </div>
                <h2 sx={styles.productTitle}>{product.name}</h2>
                <p sx={styles.productDescription}>{product.description}</p>
                <p sx={styles.productPrice}>${product.price}</p>
                <button
                  type="button"
                  onClick={() => {
                    if (isChoosedProduct) {
                      actions.removeCartItem(product)
                    } else {
                      actions.addCartItem(product)
                    }
                  }}
                  sx={{ variant: "button.primary", mx: ["auto", null, 0] }}
                >
                  {isChoosedProduct ? "Remove from cart" : "ADD TO CART"}
                </button>
              </div>
            </Container>
          </Layout>
        )
      }}
    </ProductContext.Consumer>
  )
}

const styles = {
  content: {
    display: "flex",
    flexDirection: ["column", null, null, "row"],
    py: 200,
  },
  productImageContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    width: ["1", "1", null, null, 650],
  },
  productImage: {
    width: ["40%", null, null, "70%"],
  },
  productInfo: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    width: ["1", "1", null, null, 650],
    alignItems: ["center", null, null, "flex-start"],
    mt: ["30px", null, null, "0px"],
  },
  tagsContainer: {
    display: "flex",
    fontSize: 1,
  },
  tagItem: {
    color: "#FBE067",
    textTransform: "uppercase",
  },
  productTitle: {
    fontSize: 5,
    color: "white",
    fontWeight: "bold",
    mt: "10px",
    mb: "7px",
  },
  productDescription: {
    lineHeight: "body",
    color: "light",
    mb: 0,
    width: "70%",
  },
  productPrice: {
    color: "#EEEEEE",
    fontSize: 4,
    fontWeight: "bold",
  },
}

export const query = graphql`
  query ProductQuery($slug: String!) {
    product: strapiProducts(slug: { eq: $slug }) {
      name
      price
      slug
      strapiId
      id
      description
      tags {
        id
        name
      }
    }
  }
`
