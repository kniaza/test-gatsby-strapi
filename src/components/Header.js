/** @jsx jsx */
import { jsx } from "theme-ui"
import { keyframes } from "@emotion/core"
import { useState } from "react"
import { Link } from "gatsby"
import { Container } from "../components/Grid"
import CartIcon from "../images/icons/cart.svg"
import CartCloseArrow from "../images/icons/arrow-up.svg"
import ProductIcon from "../images/products/dumy.svg"
import { ProductContext } from "../helpers/storage/productContext"
import { toast } from "react-toastify"

export default function Header() {
  const [isCartShow, setIsCartShow] = useState(false)

  return (
    <ProductContext.Consumer>
      {({ products, actions }) => (
        <header sx={styles.header}>
          {isCartShow && (
            <div
              sx={styles.blurBg}
              onClick={() => {
                setIsCartShow(false)
              }}
            />
          )}
          <Container
            sx={{
              maxWidth: [
                "100%",
                "552px",
                "732px",
                "910px",
                "1100px",
                "1320px",
                "1480px",
              ],
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <Link to="/" sx={styles.mainLink}>
              JAM SHOP
            </Link>

            <div sx={styles.cartContainer}>
              <div
                sx={styles.cartIconContainer}
                onClick={() => {
                  setIsCartShow(prevState => {
                    if (products.length === 0) {
                      toast.error("Please add product to a cart")
                      return
                    }
                    window.document.body.style.overflowY = prevState
                      ? "auto"
                      : "hidden"
                    return !prevState
                  })
                }}
              >
                <span sx={styles.cartLabel}>{products.length}</span>
                <img src={CartIcon} alt="cart" sx={styles.cartIcon} />
                <img
                  src={CartCloseArrow}
                  alt=""
                  sx={{ ...styles.cartCloseIcon, opacity: isCartShow ? 1 : 0 }}
                />
              </div>

              {isCartShow && (
                <div sx={styles.cartContent}>
                  <div sx={styles.productList}>
                    {products.map((item, index) => (
                      <div sx={styles.productItem} key={item.id}>
                        <img src={ProductIcon} alt={item.name} />
                        <span sx={styles.productTitle}>{item.name}</span>
                        <span sx={styles.productPirce}>${item.price}</span>
                      </div>
                    ))}
                  </div>
                  <div sx={styles.buyContainer}>
                    <button
                      sx={{
                        variant: "button.primary",
                        mx: ["auto", null, 0],
                        ...styles.buyButton,
                      }}
                    >
                      Submit
                    </button>
                  </div>
                </div>
              )}
            </div>
          </Container>
        </header>
      )}
    </ProductContext.Consumer>
  )
}

Header.propTypes = {}

Header.defaultProps = {}

const moveFromTop = keyframes`
  0% {
    bottom: -50px;
    opacity: 0;
  }

  100% {
    transform: -16px;
    opacity: 1;
  }
`

const styles = {
  header: {
    padding: "20px 0",
    position: "absolute",
    top: 0,
    left: 0,
    width: "1",
    background: "transparent",
    zIndex: 10,
  },
  blurBg: {
    backgroundColor: "#ffffff20",
    width: "100vw",
    height: "100vh",
    position: "absolute",
    zIndex: 9,
    top: 0,
    left: 0,
    backdropFilter: "blur(2px)",
  },
  mainLink: {
    variant: "text.link",
    color: "white",
    fontWeight: "bold",
    fontSize: 22,
    zIndex: 10,
  },
  cartIconContainer: {
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
  },
  cartContainer: {
    position: "relative",
    zIndex: 10,
    pr: 30,
  },
  cartCloseIcon: {
    width: 8,
    ml: "12px",
    transition: "opacity .2s ease-in",
  },
  cartIcon: {
    width: 30,
    height: 30,
  },
  cartLabel: {
    position: "absolute",
    backgroundColor: "accent",
    width: 12,
    height: 13,
    borderRadius: "50%",
    fontSize: "8px",
    textAlign: "center",
    lineHeight: "13px",
    color: "white",
    top: "10px",
    left: "22px",
  },
  cartContent: {
    position: "absolute",
    bottom: "-16px",
    right: 0,
    width: 281,
    height: 276,
    backgroundColor: "primary",
    transform: "translateY(100%)",
    border: "2px solid #F5F5F5",
    borderRadius: "4px",
    display: "flex",
    flexDirection: "column",
    p: 19,
    animation: `${moveFromTop} .5s ease`,
  },
  buyContainer: {
    display: "flex",
    justifyContent: "flex-end",
  },
  buyButton: {
    padding: ".6rem 1.2rem",
    fontSize: ".8rem",
    borderRadius: 0,
    textTransform: "uppercase",
  },
  productList: {
    height: 200,
    maxHeight: 200,
    overflowY: "auto",
  },
  productItem: {
    display: "flex",
    alignItems: "center",
    mb: "10px",
    img: {
      width: 30,
    },
  },
  productTitle: {
    flex: 1,
    px: 15,
    fontFamily: "body",
    fontWeight: "bold",
    color: "white",
  },
  productPirce: {
    fontFamily: "body",
    fontWeight: "bold",
    color: "white",
  },
}
