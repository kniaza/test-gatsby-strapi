/** @jsx jsx */
import React from "react"
import { jsx } from "theme-ui"
// eslint-disable-next-line import/no-named-default
import { default as SlickSlider } from "react-slick"
import ProductIcon from "../../images/products/dumy.svg"
import ArrowLeft from "../../images/icons/arrow-left.svg"
import ArrowRight from "../../images/icons/arrow-right.svg"
import { Link } from "gatsby"
import { ProductContext } from "../../helpers/storage/productContext"

const arrowStyles = {
  leftArrow: {
    fontSize: "0",
    lineHeight: "0",
    position: "absolute",
    top: "50%",
    display: "block",
    width: "65px",
    height: "65px",
    padding: "0",
    transform: "translate(0, -50%)",
    cursor: "pointer",
    color: "transparent",
    border: "none",
    outline: "none",
    background: "transparent",
    left: "-70px",
    transition: "opacity .2s ease-in",
  },
  rightArrow: {
    left: "auto",
    right: "-70px",
  },
}

const NextArrow = props => {
  const { onClick, className } = props
  const isDisabled = className.indexOf("slick-disabled") !== -1
  return (
    <div
      onClick={onClick}
      sx={{
        ...arrowStyles.leftArrow,
        ...arrowStyles.rightArrow,
        opacity: isDisabled ? "0.5" : "1",
      }}
    >
      <img src={ArrowRight} />
    </div>
  )
}

const PrevArrow = props => {
  const { onClick, className } = props
  const isDisabled = className.indexOf("slick-disabled") !== -1
  return (
    <div
      onClick={onClick}
      sx={{ ...arrowStyles.leftArrow, opacity: isDisabled ? "0.5" : "1" }}
    >
      <img src={ArrowLeft} />
    </div>
  )
}

export default function Slider({ data = [] }) {
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    draggable: false,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,

    responsive: [
      {
        breakpoint: 1500,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 770,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  }

  return (
    <ProductContext.Consumer>
      {({ products, actions }) => (
        <div>
          <SlickSlider {...settings}>
            {data.map(({ node }, index) => {
              const isChoosedProduct =
                products.filter(item => item.id === node.id).length > 0
              return (
                <div key={index}>
                  <div sx={styles.item}>
                    <Link
                      to={`/product/${node.slug}`}
                      sx={{ textDecoration: "none", cursor: "pointer" }}
                    >
                      <>
                        <div sx={styles.item_image}>
                          <img src={ProductIcon} alt="Product image" />
                        </div>
                        <p sx={styles.product_name}>{node.name}</p>
                      </>
                    </Link>
                    <div sx={styles.product_footer}>
                      <div>
                        <p sx={styles.product_description}>{node.excerpt}</p>
                      </div>
                      <div style={{ display: "flex" }}>
                        <button
                          sx={{
                            ...styles.button,
                            backgroundColor: isChoosedProduct
                              ? "light"
                              : "accent",
                          }}
                          type="button"
                          onClick={e => {
                            if (isChoosedProduct) {
                              actions.removeCartItem(node)
                            } else {
                              actions.addCartItem(node)
                            }
                          }}
                        >
                          {isChoosedProduct ? "-" : "+"}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
          </SlickSlider>
        </div>
      )}
    </ProductContext.Consumer>
  )
}

const styles = {
  item: {
    border: "3px solid #969393",
    overflow: "hidden",
    mx: 20,
    height: 273,
    borderRadius: 2,
    transition: ".15s ease-in-out",
    px: 22,
    ":hover": {
      borderColor: "accent",
    },
  },
  item_image: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    py: 30,
  },
  product_footer: {
    display: "flex",
  },
  product_name: {
    color: "white",
    fontWeight: "bold",
    mt: 0,
    mb: 2,
    fontSize: 3,
  },
  product_description: {
    fontSize: 1,
    mt: 0,
    color: "light",
  },
  button: {
    variant: "button.round",
    color: "white",
    fontSize: 3,
    fontWeight: "bold",
    ml: 2,
    alignSelf: "flex-end",
  },
}
